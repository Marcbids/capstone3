import React, {useState, useEffect, useContext} from 'react'
import {CardDeck, Card, Container, Row, Col, Table, Button, Tabs, Tab, DropdownButton ,ButtonGroup, Dropdown, Form} from 'react-bootstrap'
import AppHelper from '../../app-helper';
import View from '../../components/View';
import Head from 'next/head';
import Swal from 'sweetalert2';
import DoughnutChart from './../../components/DoughnutChart'
import Router from 'next/router';
import UserContext from '../../UserContext';


export default function index(){
	return(
		<React.Fragment>
		<Head>
			<title>Transactions</title>
		</Head>	
		<View title={ 'Transactions' }>
			<TransactionForm />
		</View>
		</React.Fragment>
	)

}

const TransactionForm = () => {

const { user, setUser } =useContext(UserContext);




	function Income (e) {
		e.preventDefault();
		setType(income)
	}
	function Payment (e) {
		e.preventDefault();
		setType(payment)
	}
	function Refresh (e) {
		e.preventDefault;
		setType("")
	}

	
	const [ transactions, setTransactions] = useState([]);
	const [ transfilter, setTransfilter] = useState([]);
	const [search, setSearch] = useState("");
	const [id, setId] = useState("");
	const [type, setType] = useState ("");
	const [key1, setKey1] = useState('transactions');

	let income = "Income";
	let payment = "Payment";

	function Delete (params) {
		Swal.fire({
			title: 	`Do you want to delete this transaction?`,
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: `Yes`,
		}).then((result) => {
			/* Read more about isConfirmed, isDenied below */
			if (result.isConfirmed) {
				const options = {
					method: 'POST',
					headers: { 
						Authorization: `Bearer ${ localStorage.getItem('token') }`,
						'Content-Type' : 'application/json' 
					},
					body: JSON.stringify({
						userId : localStorage.getItem('userId'),
						id: params
					})
						
				}
				fetch(`${ AppHelper.API_URL }/transactions/delete`, options)
				.then(AppHelper.toJSON)
				.then(data => {
				})
				Swal.fire('Saved!', '', 'success')
				Router.reload()
			}	
		})
	}
	if((type !== "Income" || type !=="Payment") && (search == "")){
		
		useEffect(() => {
			const options = {
			headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`,
			'Content-Type' : 'application/json'  }
		}
		fetch(`${ AppHelper.API_URL }/transactions/view-all`, options)
			.then(AppHelper.toJSON)
			.then(data => {
					const Trans = data.map((trans) => {
						setId(trans._id)
						return(
							<tr key={ trans._id }>
								<td>{ trans.dateOfTransaction}</td>
								<td>{ trans.description }</td>
								<td>{ trans.category }</td>
								<td>{ trans.type }</td>
								<td>{ trans.status }</td>
								<td>PHP : {trans.amount }</td>
								<td>
									<Button variant="warning">Update</Button>
									<Button variant="danger" onClick={e => Delete(trans._id)}>Delete</Button>
								</td>
							</tr>
						)
					})
					setTransactions(Trans)
				})
			
		},[type])
	} else {
		useEffect(()=>{
		const options = {
			method: 'POST',
			headers: { 'Content-Type' : 'application/json' },
			body: JSON.stringify({
				userId : localStorage.getItem('userId'),
				description : search
			})
		}
		fetch(`${ AppHelper.API_URL }/transactions/search`, options)
			.then(AppHelper.toJSON)
			.then(data => {
					const Trans = data.map((trans) => {
						setId(trans._id)
						return(
							<tr key={ trans._id }>
								<td>{ trans.dateOfTransaction}</td>
								<td>{ trans.description }</td>
								<td>{ trans.category }</td>
								<td>{ trans.type }</td>
								<td>{ trans.status }</td>
								<td>PHP : {trans.amount }</td>
								<td>
									<Button variant="warning">Update</Button>
									<Button variant="danger" onClick={e => Delete(trans._id)}>Delete</Button>
								</td>
							</tr>
						)
					})
					setTransactions(Trans)
				})
			
		},[search])
	}
	

	useEffect(() => {
		const options = {
		method: 'POST',
		headers: { 
			Authorization: `Bearer ${ localStorage.getItem('token') }`,
			'Content-Type': 'application/json' 
		},
		body: JSON.stringify({
			type: type
		})
	}
	fetch(`${ AppHelper.API_URL }/transactions/view`, options)
		.then(AppHelper.toJSON)
		.then(data => {
				const Trans = data.map((trans) => {
					setId(trans._id)
					return(
						<React.Fragment>
						<tr key={trans._id}>
							<td>{ trans.dateOfTransaction }</td>
							<td>{ trans.description }</td>
							<td>{ trans.category }</td>
							<td>{ trans.type }</td>
							<td>{ trans.status }</td>
							<td>PHP { trans.amount }</td>
							<td>
								<Button variant="warning">Update</Button>
								<Button variant="danger" onClick={e => Delete(trans._id)}>Delete</Button>
							</td>
						</tr>
						</React.Fragment>
					)
				})
				setTransfilter(Trans)
			})
	},[type])


	

	return(
		<React.Fragment>
			<Row>
				<Col xs={5}>
					<Form.Control placeholder="Search" value={search} onChange={e => setSearch(e.target.value)} />
				</Col>
				<Col xs={1}>
					<Form.Group controlId="filter">
						<DropdownButton as={ButtonGroup} title="Filter by" id="bg-nested-dropdown">
							<Dropdown.Item eventKey="1" value={income} onClick={e => Refresh(e)}>None</Dropdown.Item>
							<Dropdown.Item eventKey="2" value={income} onClick={e => Income(e)}>Income</Dropdown.Item>
							<Dropdown.Item eventKey="3" value={payment} onClick={e => Payment(e)}>Payment</Dropdown.Item>
						</DropdownButton>
					</Form.Group>
				</Col>
			</Row>
			<Tabs
			  id="controlled-tab-example"
			  activeKey={key1}
			  onSelect={(k) => setKey1(k)}
			  defaultActiveKey="transactions"
			>
			<Tab eventKey="transactions" title="Transactions">
				<Table striped bordered hover responsive="sm">
					<thead>
						<tr>
							<th>Date of Transaction</th>
							<th>Description</th>
							<th>Category</th>
							<th>Type</th>
							<th>Status</th>
							<th>Amount</th>
							<th>Actions</th>
						</tr>
					</thead>
					{
						(type !== "Income" && type !== "Payment")
						?
							<tbody>
								{ transactions }
							</tbody>
						:
							<tbody>
								{ transfilter }
							</tbody>
					}
				</Table>
			</Tab>
			<Tab eventKey="statistics" title="Statistics">
				<DoughnutChart/>
			</Tab>
			</Tabs>
		</React.Fragment>
	)

}
