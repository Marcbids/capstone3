import React, { useState } from 'react'
import {Form, Button, Row, Col} from 'react-bootstrap'
import AppHelper from '../app-helper';
import Router from 'next/router';

export default function EditProfile () {

	const [ firstName , setFirstName] = useState("");
	const [ lastName , setLastName] = useState("");
	const [ mobileNo , setMobileNo] = useState("");
	const [ password , setPassword] = useState("");

	function editprof(e){
	const options = {
		method: 'POST',
		headers: { 
			'Content-Type': 'application/json' 
		},
		body: JSON.stringify({
			userId : localStorage.getItem('userId'),
			firstName : firstName,
			lastName : lastName,
			mobileNo : mobileNo,
			password : password
		})
	}
		fetch(`${ AppHelper.API_URL }/users/edit-profile`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data)
		})
	}
	return (
		<Form onSubmit={e => editprof(e)}>
			<Row>
				<Col xs={6}>
					<Form.Group controlId="firstName">
						<Form.Label>First Name</Form.Label>
						<Form.Control type="text" placeholder="Enter first name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
					</Form.Group>
				</Col>
				<Col xs={6}>
					<Form.Group controlId="lastName">
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="text" placeholder="Enter last name" value={lastName} onChange={e => setLastName(e.target.value)}/>
					</Form.Group>
				</Col>
				<Col>
					<Form.Group controlId="mobileNo">
						<Form.Label>Mobile No</Form.Label>
						<Form.Control type="number" placeholder="Enter Mobile" value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
					</Form.Group>
				</Col>
				<Col>
					<Form.Group controlId="password">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)}/>
					</Form.Group>
				</Col>
			</Row>
			<Button variant="primary" type="submit" block>Submit</Button>
		</Form>
	)
}