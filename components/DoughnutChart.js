import {Doughnut} from 'react-chartjs-2'
import React,{useState,useEffect} from 'react'
import {Table} from 'react-bootstrap'
import AppHelper from '../app-helper';
import LineChart from 'react-linechart';
import '../node_modules/react-linechart/dist/styles.css';

export default function DoughnutChart({}){

	const [income, setIncome] = useState(0)
	const [expense, setExpense] = useState(0)



	useEffect(() => {
		const options = {
		headers: { Authorization: `Bearer ${ localStorage.getItem('token') }` }
	}
		fetch(`${ AppHelper.API_URL }/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			setIncome(data.totalIncome)
			setExpense(data.totalExpense)
	})
	},[])


	return(
		<React.Fragment>
		<Table striped bordered hover responsive="sm">
				<thead>
					<tr>
					 	<th>Total Income : {income}</th>
					 	<th>Total Expense : {expense}</th>
				 	</tr>
			 	</thead>
		</Table>
		<Doughnut
				data={{
					datasets:[{
						data:[income, expense],
						backgroundColor:["green", "red"]
					}],
					labels:["Total Income", "Total Expense"]
				}}
				redraw={true}
			/>

		</React.Fragment>
		)
}
