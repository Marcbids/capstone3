import React, { useState, useContext, useEffect } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext';
// import users from '../../data/users';
import View from '../../components/View';
import AppHelper from '../../app-helper';

export default function index(){

	return(
		<View title={ 'Login' }>
			<Row className="justify-content-center">
				<Col xs md="6">
					<h3>Login</h3>
					<LoginForm />
				</Col>
			</Row>
		</View>
	)
}

const LoginForm = () => {

	// consume UserContext and destructure it to access the user and setUser defined in the App component
	const { user, setUser } =useContext(UserContext);
	useEffect(()=>{
		localStorage.setItem('userId', user.id)
	},[user])
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	

	function authenticate(e){

		e.preventDefault();
		

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		}

		// "{ email: email, password: password }"

		fetch(`${ AppHelper.API_URL }/users/login`, options)
		.then(AppHelper.toJSON)
		.then(data => {

			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);

			} else {
				if(data.error === 'incorrect-password') {
					Swal.fire('Authentication Failed', 'Password is incorrect', 'error')
				} else if (data.error === 'does-not-exist') {
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }

			}

		})

	}

	const authenticateGoogleToken = (response) => {	   
		console.log(response)     
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch (`${ AppHelper.API_URL }/users/verify-google-id-token`, options)
        .then(AppHelper.toJSON)
        .then(data => {
        	console.log(data)
            if (typeof data.accessToken !== 'undefined'){
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } 
            
        })
    };

	const retrieveUserDetails = (accessToken) => {
		

		const options = {
			headers: { Authorization: `Bearer ${ accessToken }`,
			'Content-Type' : 'application/json'  }
		}

		fetch(`${ AppHelper.API_URL }/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			setUser({ id: data._id, isAdmin: data.isAdmin});
			Swal.fire({
				title: 	`Successfully logged in!`,
				icon: 'success',
				confirmButtonText: `ok`,
			}).then((result) => {
				if (result.isConfirmed) {
					Router.push('/profile');
				}
			})
			
			

		})

	}

	return(
		//Create a form for user login, with labels and inputs for email and password, as well as a button for form submission
		<React.Fragment>
			<Head>
				<title>Authentication</title>
			</Head>
			<Container>
				<Form onSubmit={e => authenticate(e)}>

					<Form.Group controlId="email">
						<Form.Label>Email:</Form.Label>
						<Form.Control type="email" placeholder="Email Address" value={email} onChange={e => setEmail(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="password">
		                <Form.Label>Password:</Form.Label>
		                <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required/>
		            </Form.Group>

		            <Button variant="primary" type="submit" block>Submit</Button>

		            <GoogleLogin 
		            	clientId="61453311285-2i01atc557lv9imk2rb1vqevhvt0c2o7.apps.googleusercontent.com"
		            	buttonText="Login"
		            	onSuccess={ authenticateGoogleToken }
		            	onFailure={ authenticateGoogleToken }
		            	cookiePolicy={ 'single_host_origin' }
		            	className="w-100 text-center d-flex justify-content-center"
		            />

				</Form>
			</Container>
		</React.Fragment>
	)
}
