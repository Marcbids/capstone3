import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import AppHelper from '../../app-helper';
import Head from 'next/head'
import Router from 'next/router';

import Swal from 'sweetalert2';

export default function index() {
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstname, setFirstname] = useState('');
	const [lastname, setLastname] = useState('');
	const [number, setNumber] = useState('');
	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {
		e.preventDefault();

		const options = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				firstName: firstname,
				lastName: lastname,
				email: email,
				mobileNo: number,
				password: password1
			})
		}


		fetch(`${ AppHelper.API_URL }/users/`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			Swal.fire(
				'Great!',
				`Successfully Registered!`,
				'success'
				)
			Router.push(`/`)
		})

	}
	// form => registerUser()

	useEffect(() => {
		if((email !== '' && password1 !== '' && password2 !== '' && firstname !== '' && lastname !== '' && number !== '') && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password1, password2, firstname, lastname, number]);

	return (
		<React.Fragment>
		<Head>
				<title>Authentication</title>
		</Head>
		<Form onSubmit={e => registerUser(e)} className="col-lg-4 offset-lg-4 my-5">
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter First Name"
					value={firstname}
					onChange={e => setFirstname(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Last Name"
					value={lastname}
					onChange={e => setLastname(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control 
					type="number"
					placeholder="Enter Mobile Number"
					value={number}
					onChange={e => setNumber(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{isActive
				?
				<Button 
					className="bg-primary"
					type="submit"
					id="submitBtn"
				>
					Submit
				</Button>
				:
				<Button 
					className="bg-danger"
					type="submit"
					id="submitBtn"
					disabled
				>
					Submit
				</Button>
			}
	
		</Form>
		</React.Fragment>
	)
};