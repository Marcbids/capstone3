import React, { useContext } from 'react';
import Link from 'next/link';
import { Navbar, Nav } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function NavBar() {

    const { user } = useContext(UserContext);

    return (
        <Navbar bg="light" expand="lg">
                <h3 className="navbar-brand">Budget management</h3>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    {(user.id !== undefined)
                        ? (user.isAdmin === true)
                            ? 
                            <React.Fragment>
                                <Link href="/logout">
                                    <a className="nav-link" role="button">Logout</a>
                                </Link>
                            </React.Fragment>
                            : 
                            <React.Fragment>
                            <Link href="/profile">
                                <a className="nav-link" role="button">Profile</a>
                            </Link>
                            <Link href="/transactions">
                                <a className="nav-link" role="button">Transactions</a>
                            </Link>
                            <Link href="/logout">
                                <a className="nav-link" role="button">Logout</a>
                            </Link>
                            </React.Fragment>
                        : 
                        <React.Fragment>
                            <Link href="/login">
                                <a className="nav-link" role="button">Login</a>
                            </Link>
                            <Link href="/register">
                                <a className="nav-link" role="button">Register</a>
                            </Link>
                        </React.Fragment>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
