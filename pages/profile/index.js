import React, { useContext, useState, useEffect } from 'react'
import {CardDeck, Card, Container, Row, Col, Table, Button, Tabs, Tab} from 'react-bootstrap'
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';
import View from '../../components/View';
import Head from 'next/head';
import AddTrans from './../../components/addTrans'
import EditProfile from './../../components/editProfile'

export default function index() {

	return(
		<React.Fragment>
		<Head>
			<title>Profile</title>
		</Head>	
		<View title={ 'Profile' }>

			<ProfileForm />
		</View>
		
		</React.Fragment>
	)
	
}


const ProfileForm = () => {
	const [ token, setToken ] = useState("");
	const [ profile, setProfile] = useState("");

	const [key, setKey] = useState('home');
	

	useEffect(() => {
		const options = {
		headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`,
		'Content-Type' : 'application/json'  }
	}
		fetch(`${ AppHelper.API_URL }/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			setProfile(
				<Card>
					<Card.Body>
					<Card.Title>{data.firstName} {data.lastName}</Card.Title>
						<Card.Text>
						 	<p>Mobile # : {data.mobileNo} </p>
						 	<p>Email address : {data.email} </p>
						 	<h4>PHP : {data.money}</h4>
						</Card.Text>
					</Card.Body>
				</Card>
				)	
	})
	},[key])

	

	return (
		<Tabs
		  id="controlled-tab-example"
		  activeKey={key}
		  onSelect={(k) => setKey(k)}
		>
		<Tab eventKey="home" title="Profile">
		    {profile}
		</Tab>
		<Tab eventKey="transactions" title="Add Transaction">
		    <AddTrans />
		</Tab>
		<Tab eventKey="editProfile" title="Edit Profile">
		    <EditProfile />
		</Tab>
		</Tabs>
	);
}	
