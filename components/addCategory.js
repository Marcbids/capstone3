import React, {useState } from 'react'
import {Button, Modal, Form, Row, Col, Container} from 'react-bootstrap'
import AppHelper from '../app-helper';
import Router from 'next/router';

import Swal from 'sweetalert2';

export default function AddCategory() {
  	const [show, setShow] = useState(false);

	const [type, setType] = useState("Payment");
	const [category, setCategory] =useState("")

  	function editprof(){
	  	const option = {
	  		method: 'POST',
	  		headers: {
	  			'Content-Type': 'application/json'
	  		},
	  		body: JSON.stringify({
	  			userId : localStorage.getItem('userId'),
	  			type : type,
	  			category : category
	  		})
	  	}
	  	fetch(`${ AppHelper.API_URL }/category/`, option)
	  	.then(AppHelper.toJSON)
		.then(data => {
		})	
  	}
  	const closeModal = () => setShow(false); 
  	const showModal = () => setShow(true);
  	function saveModal(){
  		setShow(false); 
  		editprof();
  		Swal.fire({
			title: 	`Category added`,
			icon: 'info',
			confirmButtonText: `ok`,
		}).then((result) => {
			if (result.isConfirmed) {
  				Router.reload()
			}
		})
  	}


  	return (
	    <React.Fragment>
		      <Button variant="primary" onClick={showModal}>
		        Add Category
		      </Button>

	      	<Modal show={show} onHide={showModal}>
	        	<Modal.Header closeButton>
	          	<Modal.Title>Add Category</Modal.Title>
		        </Modal.Header>
		        <Form>
		        <Modal.Body>
		        	<Container>
			        	
						<Row>
							<Col xs={6}>
								<Form.Group controlId="type">
					                <Form.Label>Type:</Form.Label>
					                <Form.Control as="select" placeholder="Enter type" value={type} onChange={e => setType(e.target.value)} required>
					                	<option>Payment</option>
					                	<option>Income</option>
			                		</Form.Control>
					            </Form.Group>
				            </Col>
							<Col xs={6}>
								<Form.Group controlId="category">
									<Form.Label>Category</Form.Label>
									<Form.Control type="text" placeholder="Enter last name" value={category} onChange={e => setCategory(e.target.value)}/>
								</Form.Group>
							</Col>
						</Row>						
					</Container>
		        </Modal.Body>

		        <Modal.Footer>
		          	<Button variant="secondary"  onClick={closeModal}>
		            	Close
		          	</Button>
		          	<Button variant="primary" onClick={saveModal}>
		            	Save Changes
		          	</Button>
		        </Modal.Footer>
		        </Form>
	      	</Modal>
	    </React.Fragment>
  );
}