import React, {useEffect, useState, useContext} from 'react'
import {Container, Form, Button, Row, Col } from 'react-bootstrap'
import AppHelper from '../app-helper';
import Router from 'next/router';
import AddCategory from './addCategory'

import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function AddTrans(){

	const { user, setUser } =useContext(UserContext);

	const [description, setDescription] = useState("");
	const [type, setType] = useState("");
	const [category, setCategory] =useState("")
	const [categoryoption, setCategoryoption] =useState("")
	const [amount, setAmount] = useState("");
	const [token, setToken] = useState("")
	const [money, setMoney] = useState("");


	useEffect(()=>{
		const cors = {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				userId : user.id,
				type: type
			})
		}

		fetch(`${ AppHelper.API_URL }/category/view-category`, cors)
		.then(AppHelper.toJSON)
		.then(data => {
			const categories = data.map((cat) => {
				return(
					<option>{cat.category}</option>
				)
			})
			setCategory(categories)
		})
	},[type])



	function addTrans(e){
		e.preventDefault();

			const options = {
				method: 'POST',
				headers: { 
					Authorization: `Bearer ${ localStorage.getItem('token') }`,
	 				'Content-Type': 'application/json' 
	 			},
				body: JSON.stringify({
					description: description,
					type: type,
					amount: amount,
					userId: user.id,
					category: categoryoption
				})
			}

			fetch(`${ AppHelper.API_URL }/users/transactions`, options)
			.then(AppHelper.toJSON)
			.then(data => {
				if(data){
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Transaction success',
						showDenyButton: true,
						confirmButtonText: `Add another one?`,
						denyButtonText: `No`
					}).then((result) => {
						if (result.isConfirmed) {
						 setDescription("");
						 setType("Payment");
						 setAmount("")
						} else if (result.isDenied) {
						Router.reload()
						}
						})
					
					//redirect to add course
				}else{
					//error occured in registration
					alert("Something Went wrong")
				}
			})

	}

	return(
		<React.Fragment>
			<Container>
				<Form onSubmit={e => addTrans(e)}>

					<Form.Group controlId="description">
						<Form.Label>Description:</Form.Label>
						<Form.Control type="text" placeholder="Enter Description" value={description} onChange={e => setDescription(e.target.value)} required/>
					</Form.Group>

					<Form.Group controlId="type">
		                <Form.Label>Type:</Form.Label>
		                <Form.Control as="select" placeholder="Enter type" value={type} onChange={e => setType(e.target.value)} required>
		                	<option>{null}</option>
		                	<option>Payment</option>
		                	<option>Income</option>
                		</Form.Control>
		            </Form.Group>

		            <Form.Group controlId="category">
		                <Form.Label>Category:</Form.Label>
		                <Row>
		                <Col>
		                <Form.Control as="select" placeholder="Enter type" value={categoryoption} onChange={e => setCategoryoption(e.target.value)} required>
		                	<option>{null}</option>
		                	{category}
                		</Form.Control>
                		</Col>
                		<Col>
                		<AddCategory />
                		</Col>
                		</Row>
		            </Form.Group>

		            <Form.Group controlId="amount">
						<Form.Label>Amount:</Form.Label>
						<Form.Control type="number" placeholder="Enter Amount" value={amount} onChange={e => setAmount(parseInt(e.target.value))} required/>
					</Form.Group>

		            <Button variant="primary" type="submit" block>Submit</Button>

				</Form>
			</Container>
		</React.Fragment>
	)
}